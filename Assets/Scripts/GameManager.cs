﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private InputManager inputManager;
    private HUD hud;
    private Kirby kirby;
    private Spawner spawner;
    public AudioSource audio;

    public bool isPaused;
    public bool gameover;
    public bool startGame = false;    
    private float timeCounter;

	void Start ()
    {
        isPaused = false;
        startGame = false;

        inputManager = new InputManager();
        inputManager.Initialize();

        hud = GameObject.FindGameObjectWithTag("HUD").GetComponent<HUD>();
        hud.ClosePausePanel();
        hud.CloseGameoverPanel();
        hud.CloseGameplayPanel();
        hud.OpenTitlePanel();
        audio.Play();

        kirby = GameObject.FindGameObjectWithTag("Player").GetComponent<Kirby>();
        kirby.Initialize();
        spawner = GameObject.FindGameObjectWithTag("Spawner").GetComponent<Spawner>();
        spawner.Initialize();
        audio.Stop();

        timeCounter = 0;
        hud.UpdateTimeText(timeCounter);

        
    }
    void Update()
    {
        if(!startGame) return;
        if(gameover)
        {
            kirby.DeadUpdate();
            return;
        }

        inputManager.ReadInput(); //Update Input

        kirby.MyUpdate(); //Update kirby
        spawner.MyUpdate(); //Update spawner

        timeCounter += Time.deltaTime;
        hud.UpdateTimeText(timeCounter);
    }
    public void PauseGame()
    {
        isPaused = !isPaused;

        if(isPaused)
        {
            Time.timeScale = 0;
            hud.OpenPausePanel();
        }
        else
        {
            Time.timeScale = 1;
            hud.ClosePausePanel();
        }
    }
    public void StartGame()
    {
        startGame = true;
        hud.CloseTitlePanel();
        hud.OpenGameplayPanel();
        audio.Play();
    }
    public void GameOver()
    {
        gameover = true;

        SaveGame();

        hud.UpdateGameOverPanel();
        hud.OpenGameoverPanel();
        audio.Stop();
    }
    void SaveGame()
    {
        float record = 0;

        if(PlayerPrefs.HasKey("Record"))
        {
            record = PlayerPrefs.GetFloat("Record");
            if(timeCounter > record) record = timeCounter;
        }
        else record = timeCounter;

        PlayerPrefs.SetFloat("Record", record);
        PlayerPrefs.SetFloat("Score", timeCounter);
    }
    public void LoadScene(int num)
    {
        SceneManager.LoadScene(num);
    }
}
