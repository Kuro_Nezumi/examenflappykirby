﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour {

    public GameObject pausePanel;
    public GameObject gameoverPanel;
    public GameObject gameplayPanel;
    public GameObject titlePanel;

    public Text timetext;
    public Text scoreText;
    public Text recordText;

    public void OpenPausePanel() { pausePanel.SetActive(true); }
    public void ClosePausePanel() { pausePanel.SetActive(false); }

    public void OpenGameoverPanel() { gameoverPanel.SetActive(true); }
    public void CloseGameoverPanel() { gameoverPanel.SetActive(false); }

    public void OpenGameplayPanel() { gameplayPanel.SetActive(true); }
    public void CloseGameplayPanel() { gameplayPanel.SetActive(false); }

    public void OpenTitlePanel() { titlePanel.SetActive(true); }
    public void CloseTitlePanel() { titlePanel.SetActive(false); }

    public void UpdateTimeText(float timeCounter)
    {
        Debug.Log("UpdateTimeText");
        timetext.text = ConvertFloatToTime(timeCounter);   
    }

    public void UpdateGameOverPanel()
    {
        scoreText.text = "Score: " + ConvertFloatToTime(PlayerPrefs.GetFloat("Score"));
        recordText.text = "Record: " + ConvertFloatToTime(PlayerPrefs.GetFloat("Record"));
    }

    string ConvertFloatToTime(float time)
    {
        float h = time / 3600;
        time %= 3600;
        float m = time / 60;
        time %= 60;
        float s = time;

        return h.ToString("00") + ":" + m.ToString("00") + ":" + s.ToString("00");
    }

}
