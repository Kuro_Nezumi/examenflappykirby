﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject objToSpawn;
    public GameObject objToSpawn2;

    private float timeSpawn;
    public float minTime, maxTime;

    public float offsetY;

	// Use this for initialization
	public void Initialize()
    {
        timeSpawn = minTime;
    }
	
	// Update is called once per frame
	public void MyUpdate ()
    {
        if(timeSpawn <= 0)
        {
            Spawn();
        }
        else timeSpawn -= Time.deltaTime;
	}

    void Spawn()
    {
        timeSpawn = Random.Range(minTime, maxTime);

        Vector2 spawnpos = this.transform.position;
        spawnpos.y += Random.Range(-offsetY, offsetY);

        Instantiate(objToSpawn, spawnpos, Quaternion.identity);
    }

}
